import numpy as np

# Class representing an object in three-dimensional space (i.e. a cube)
# Based off of example class given in:
# http://www.petercollingridge.co.uk/pygame-3d-graphics-tutorial/
class Wireframe:
    def __init__(self):
        self.nodes = np.zeros((0, 4))
        self.faces = []
        self.location = (0,0,0)

    def addNodes(self, node_array):
        ones_column = np.ones((len(node_array), 1))
        ones_added = np.hstack((node_array, ones_column))
        self.nodes = np.vstack((self.nodes, ones_added))

    def addFaces(self, faceList):
        self.faces += faceList

    # Find center point of face
    def getFaceCenter(self, face):
        nodeNums = [x for x in face]
        nodes = [np.array(self.nodes[n]) for n in nodeNums]
        center = np.array([0,0,0])
        for node in nodes:
            center =  np.add(center, node[:3])
        center /= len(nodes)
        return center

    # Find distance of face from origin (0,0,0)
    def getFaceDistance(self, face):
        center = self.getFaceCenter(face)
        return (center[0]**2 + center[1]**2 + center[2]**2)**.5
    
    # Sorts faces by distance from origin in descending order (i.e. furthest face is element 0)
    def sortFaces(self):
        self.faces.sort(key=lambda x: self.getFaceDistance(x), reverse=True)
    
    # Gets distance along z axis of center of wireframe from origin
    def getZValue(self):
        return sum([node[2] for node in self.nodes]) / len(self.nodes)

    # Gets distance from center of wireframe to origin
    def getDistance(self):
        center = self.findCenter()
        return (center[0]**2 + center[1]**2 + center[2]**2)**.5

    # Finds center of wireframe
    def findCenter(self):
        num_nodes = len(self.nodes)
        meanX = sum([node[0] for node in self.nodes]) / num_nodes
        meanY = sum([node[1] for node in self.nodes]) / num_nodes
        meanZ = sum([node[2] for node in self.nodes]) / num_nodes
        
        return (meanX, meanY, meanZ)
    
    # finds distance from center of wireframe to any given point
    def getDistanceFromPoint(self, point):
        center = self.findCenter()
        return ((point[0] - center[0])**2 + (point[1] - center[1])**2 + (point[2] - center[2])**2)**.5

    # Apply matrix transforms
    def transform(self, matrix, translation=False, deltaVector=None):
        if translation == True:
            self.location = [x + y for x, y in zip(self.location, deltaVector)]
        self.nodes = np.dot(self.nodes, matrix)

#####

# Transformation matricies

def translationMatrix(dx=0, dy=0, dz=0):    
    return np.array([[1,0,0,0],
                     [0,1,0,0],
                     [0,0,1,0],
                     [dx,dy,dz,1]])


def scaleMatrix(sx=0, sy=0, sz=0):    
    return np.array([[sx, 0,  0,  0],
                     [0,  sy, 0,  0],
                     [0,  0,  sz, 0],
                     [0,  0,  0,  1]])

def rotateXMatrix(radians):
    c = np.cos(radians)
    s = np.sin(radians)
    return np.array([[1, 0, 0, 0],
                     [0, c,-s, 0],
                     [0, s, c, 0],
                     [0, 0, 0, 1]])

def rotateYMatrix(radians):   
    c = np.cos(radians)
    s = np.sin(radians)
    return np.array([[ c, 0, s, 0],
                     [ 0, 1, 0, 0],
                     [-s, 0, c, 0],
                     [ 0, 0, 0, 1]])

def rotateZMatrix(radians):
    c = np.cos(radians)
    s = np.sin(radians)
    return np.array([[c,-s, 0, 0],
                     [s, c, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, 1]])



#####

# Returns a wireframe object representing a cube
def getCube():
    cube = Wireframe()
    cube_nodes = [(x,y,z) for x in (0,1) for y in (0,1) for z in (0,1)]
    cube.addNodes(np.array(cube_nodes))

    cube.addFaces([(0,1,3,2)])
    cube.addFaces([(0,2,6,4)])
    cube.addFaces([(6,7,5,4)])
    cube.addFaces([(3,7,5,1)])
    cube.addFaces([(0,1,5,4)])
    cube.addFaces([(2,3,7,6)])

    return cube
