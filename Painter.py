import MatrixModule, pygame
import numpy as np
from pygame.locals import *


FOV = 600
SCREEN_DIMENSIONS = (600, 600)
X_CENTER = SCREEN_DIMENSIONS[0]/2
Y_CENTER = SCREEN_DIMENSIONS[1]/2
LIGHT_SOURCE = (0,0,-10)
LIGHT_FALLOFF_CONSTANT = .03


def to2D(x,y,z):
    return (x*FOV/z + X_CENTER, y*FOV/z + Y_CENTER)
	
def getColor(box, *nodes):
    # Calculate face normal
    p1 = box.nodes[nodes[0]][:3]
    p2 = box.nodes[nodes[1]][:3]
    p3 = box.nodes[nodes[2]][:3]
    v1 = np.subtract(p2, p1)
    v2 = np.subtract(p3, p1)
    cross = np.cross(v1,v2)
    norm = cross / np.linalg.norm(cross)
    
    # Calculate color of face
    faceCenterPoint = (np.array(p1) + np.array(p2) + np.array(p3)) / 3
    lightDirection = np.subtract(LIGHT_SOURCE, faceCenterPoint)
    lightNorm = lightDirection / np.linalg.norm(lightDirection)
    factor = max(0, np.cos(np.dot(norm,lightNorm))) /    \
                     (LIGHT_FALLOFF_CONSTANT * (1 + faceCenterPoint[2]**2))
    c = min(200 * factor, 200)

    return (c,c,c)


# Paints maze to surface
def paintMaze(surface, maze):
    # Loops through all boxes
    for box in maze.boxes:
        # skips box if the box is too far away
        if box.getDistance() > 25:
            continue
        
        counter = 0
        for n1,n2,n3,n4 in box.faces:
            counter += 1
            # only paint closest 3 faces
            if counter < 4:
                continue
        
            # Paints face if visible
            if box.nodes[n1][2] > 0 and box.nodes[n2][2] > 0 \
               and box.nodes[n3][2] > 0 and box.nodes[n4][2] > 0:
                try:
                    pygame.draw.polygon(surface, getColor(box, n1,n2,n3),
                                        (to2D(*box.nodes[n1][:3]),
                                        to2D(*box.nodes[n2][:3]),
                                        to2D(*box.nodes[n3][:3]),
                                        to2D(*box.nodes[n4][:3])))
                except:
                    pass
