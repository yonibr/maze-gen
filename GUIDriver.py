import numpy as np
import pygame, sys, math, time
from pygame.locals import *
from MazeGenerator import *
from Painter import *


# Constants
RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
WHITE = (255,255,255)
BLACK = (0,0,0)
MAZE_WIDTH = 25
MAZE_HEIGHT = 25



# Sets up pyGame
surface = pygame.display.set_mode(SCREEN_DIMENSIONS, 0, 32)
pygame.display.set_caption('Maze')
pygame.init()
pygame.font.init()
font = pygame.font.SysFont("monospace", 35)



# Creates Maze
maze = Maze(MAZE_WIDTH, MAZE_HEIGHT)



# Used for displaying frames per second
current_milli_time = lambda: int(round(time.time() * 1000))

totalTime = frames = fps = 0
lastTime = curTime = current_milli_time()

def calcFPS():
    global lastTime, curTime, totalTime, frames, fps
    lastTime = curTime
    curTime = current_milli_time()
    totalTime += curTime - lastTime
    if totalTime > 1000:
        totalTime -= 1000
        fps = frames
        frames = 0
    frames += 1

    f = font.render("FPS:  " + str(fps), True,(255,0,0))
    surface.blit(f, (10, 10))




# Manages user input
def handleInput():
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    keys = pygame.key.get_pressed()

    if keys[K_ESCAPE]:
        pygame.quit()
        sys.exit()

    if keys[K_SPACE]:
        maze.moveForward()

    if keys[K_w]:
        maze.rotateMazeInDirection(UP)
    if keys[K_s]:
        maze.rotateMazeInDirection(DOWN)
    if keys[K_a]:
        maze.rotateMazeInDirection(LEFT)
    if keys[K_d]:
        maze.rotateMazeInDirection(RIGHT)
    
# Handles all rendering
def render():
    pygame.draw.rect(surface, BLACK, (0, 0, SCREEN_DIMENSIONS[0], SCREEN_DIMENSIONS[1]))
    paintMaze(surface, maze)
    calcFPS()
    pygame.display.update()

# Game loop
def loop():
    handleInput()
    render()


# Starts loop
while True:
    loop()
