from random import randint
import math
from MatrixModule import *

# Directions
LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3

class Maze:
    def __init__(self, *dimensions):
        # Number of dimensions
        self.type = len(dimensions)
        
        # If 2D (everything is 2D at the moment)
        if self.type == 2:
            
            # Center maze onscreen
            xShift = dimensions[0]/2
            zShift = 0
            
            # Get maze values
            booleanValues = get2DMaze(dimensions)

            # Places cubes in proper z location
            while zShift == 0:
                for i, bool in enumerate(booleanValues[randint(0,dimensions[0]-1)]):
                    if bool == False:
                        zShift = -i + .1
        
            # Sets up boxes
            self.boxes = []
            for i, row in enumerate(booleanValues):
                for c, value in enumerate(row):
                    if value == True:
                        self.boxes.append(getCube())
                        # Places cubes
                        self.boxes[-1].transform(translationMatrix(i-xShift, -.5, c+zShift),
                                                 translation=True,
                                                 deltaVector=(i-xShift, -.5, c+zShift))
                        self.boxes[-1].sortFaces()
            
            self.rotateMazeInDirection(LEFT, math.pi/2)
            self.sortBoxesByDistance()

    # sorts the boxes by distance to origin (0,0,0)
    def sortBoxesByDistance(self):
        self.boxes.sort(key=lambda x: x.getDistance(), reverse=True)


    # determines normal for given face
    def getFaceNormal(self, face):
        p1 = self.boxes[-1].nodes[face[0]][:3]
        p2 = self.boxes[-1].nodes[face[1]][:3]
        p3 = self.boxes[-1].nodes[face[2]][:3]
        v1 = np.subtract(p2, p1)
        v2 = np.subtract(p3, p1)
        cross = np.cross(v1,v2)
        return np.divide(cross, np.linalg.norm(cross))


    # Finds intersection point between the vector of the player and the nearest face of the given box
    def getIntersectPoint(self, box):
        boxCenter = box.findCenter()
        face = box.faces[-1]
        normal = self.getFaceNormal(face)
        p4 = box.nodes[face[3]][:3]

        # prevent divide by 0 errors:
        if boxCenter[0] == 0:
            boxCenter[0] = 0.001
        d = p4[0] * normal[0] + p4[1] * normal[1] + p4[2] * normal[2]
        x = d/.00001
        try:
            x = d/(normal[0] + boxCenter[1]*normal[1]/boxCenter[0] + boxCenter[2]*normal[2]/boxCenter[0])
        except:
            pass
        y = boxCenter[1] * x / boxCenter[0]
        z = boxCenter[2] * x / boxCenter[0]
        return (x,y,z)



    # Determines the closest a player can get to given box
    def getAllowedDistance(self, box):
        intersectPoint = self.getIntersectPoint(box)
        boxCenter = box.findCenter()
        return ((boxCenter[0] - intersectPoint[0])**2 + (boxCenter[1] - intersectPoint[1])**2 + (boxCenter[2] - intersectPoint[2]) **2)**.5



    # Determines if the player can move forward. If the player can move, then move the player forward.
    def moveForward(self, dZ=.05):
        self.sortBoxesByDistance()
        for box in self.boxes:
            zVal = box.getZValue()
            if zVal < 0 or zVal > 4:
                continue
            allowedDistance = self.getAllowedDistance(box)
            if box.getDistanceFromPoint((0,0,2*dZ)) < allowedDistance:
                print("Collided!")
                return

        for box in self.boxes:
            box.transform(translationMatrix(dz=-dZ), translation=True,
                          deltaVector=(0,0,-dZ))


    # Rotates all boxes in maze in the given direction
    def rotateMazeInDirection(self, direction, radians=0.1):
        if direction == LEFT:
            for box in self.boxes:
                box.transform(rotateYMatrix(-radians))
                box.sortFaces()
        elif direction == RIGHT:
            for box in self.boxes:
                box.transform(rotateYMatrix(radians))
                box.sortFaces()
        elif direction == UP:
            for box in self.boxes:
                box.transform(rotateXMatrix(radians))
                box.sortFaces()
        elif direction == DOWN:
            for box in self.boxes:
                box.transform(rotateXMatrix(-radians))
                box.sortFaces()





class Wall2D:
    def __init__(self, x, y, adjacentCell):
        self.x = x
        self.y = y
        self.adjacentCell = adjacentCell


# Gets walls adjacent to cell
def get2DCellWalls(dimensions, location):
    walls = []
    if location[0] > 2:
        walls.append(Wall2D(location[0] - 1, location[1], location))
    if location[0] < dimensions[0] - 3:
        walls.append(Wall2D(location[0] + 1, location[1], location))
    if location[1] > 2:
        walls.append(Wall2D(location[0], location[1] - 1, location))
    if location[1] < dimensions[1] - 3:
        walls.append(Wall2D(location[0], location[1] + 1, location))
    return walls


# Returns two dimensional maze calculated using a randomized implementation of Prim's Algorithm
def get2DMaze(dimensions):
    # Determine dimensions of maze
    x = dimensions[0]
    y = dimensions[1]
    # Initializes two-dimensional boolean list
    grid = [[True]*y for row in range(x)]
    # Determines starting cell
    cell = [randint(3,x-3), randint(3,y-3)]
    # Sets starting cell to false
    grid[cell[0]][cell[1]] = False
    # Finds adjacent walls
    walls = get2DCellWalls(dimensions, cell)
    # While there are still open walls
    while len(walls) > 0:
        # Picks random wall
        rand = randint(0, len(walls) - 1)
        wall = walls[rand]
        # Determines if the cell opposite to the current cell is a wall or part of the maze
        cell = wall.adjacentCell
        oppositeCell = (wall.x - cell[0] + wall.x, wall.y - cell[1] + wall.y)
        # If the opposite cell is a wall, make it and the cell adjacent to the current cell part of the maze.
        # Then remove the current wall from the list of walls.
        if grid[oppositeCell[0]][oppositeCell[1]] == True:
            walls.pop(rand)
            grid[wall.x][wall.y] = False
            grid[oppositeCell[0]][oppositeCell[1]] = False
            walls.extend(
                get2DCellWalls(dimensions, (oppositeCell[0], oppositeCell[1])))
        # Otherwise just remove the wall from the list of walls
        else:
            walls.pop(rand)
    return grid
